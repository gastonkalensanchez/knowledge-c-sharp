﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL;


namespace TP_Final_Patadigmas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public Jugador jugador1 = new Jugador() {Vida = 10, Escudo = 10, Mazo = new Mazo()};
        public Jugador jugador2 = new Jugador() {Vida = 10, Escudo = 10, Mazo = new Mazo()};
        

        public bool turnoJ1 = true;
        public int rondasJugadas = 0;

        public delegate void Delegado();
        Delegado delegado;

        private void Form1_Load_1(object sender, EventArgs e)
        {
            gdrJugador1.AllowUserToDeleteRows = false; // no puede borrar filas
            gdrJugador1.EditMode = DataGridViewEditMode.EditProgrammatically;
            gdrJugador1.SelectionMode = DataGridViewSelectionMode.FullRowSelect; // que seleccione toda la fila si apreta un casillero 
            gdrJugador1.MultiSelect = false;// que no se puedan seleccionar ams de una fila

            gdrJugador2.AllowUserToDeleteRows = false; // no puede borrar filas
            gdrJugador2.EditMode = DataGridViewEditMode.EditProgrammatically;
            gdrJugador2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            gdrJugador2.MultiSelect = false;

            progressBar1.Value = jugador1.Vida;
            progressBar2.Value = jugador1.Escudo;
            progressBar3.Value = jugador2.Vida;
            progressBar4.Value = jugador2.Escudo;

            #region CreacionRandom
            var rand1 = new Random(); // genero de forma random el tipó de enemigo para el mazo del jugador 1
            for (int i = 0; i < 10; i++)
            {
                int r = rand1.Next(0, 4);
                if (r == 0)
                {
                    jugador1.Mazo.Agregar(new Pato_Verde());
                }
                if (r == 1)
                {
                    jugador1.Mazo.Agregar(new Pato_Amarillo());
                }
                if (r == 2)
                {
                    jugador1.Mazo.Agregar(new Pato_Azul());
                }
                if (r == 3)
                {
                    jugador1.Mazo.Agregar(new Pato_Rojo());
                }

            }
            var rand2 = new Random();

            for (int i = 0; i < 10; i++) // genero de forma random el tipó de enemigo para el mazo del jugador 2
            {
                int r = rand2.Next(0, 4);
                if (r == 0)
                {
                    jugador2.Mazo.Agregar(new Pato_Verde());
                }
                if (r == 1)
                {
                    jugador2.Mazo.Agregar(new Pato_Amarillo());
                }
                if (r == 2)
                {
                    jugador2.Mazo.Agregar(new Pato_Azul());
                }
                if (r == 3)
                {
                    jugador2.Mazo.Agregar(new Pato_Rojo());
                }

            }
            #endregion    
            delegado = TurnoJ1;
            Actualizar();
        }
        private void Actualizar()
        {
            gdrJugador1.DataSource = null;
            gdrJugador1.DataSource = jugador1.Mazo.lCarta;
            gdrJugador2.DataSource = null;
            gdrJugador2.DataSource = jugador2.Mazo.lCarta;


            progressBar1.Value = jugador1.Vida;
            progressBar2.Value = jugador1.Escudo;
            progressBar3.Value = jugador2.Vida;
            progressBar4.Value = jugador2.Escudo;
        }

        private void SeleccionarCarta2J1_Click(object sender, EventArgs e)
        {
            
            if(turnoJ1)
            {

                Carta pCarta = (Carta)gdrJugador1.SelectedRows[0].DataBoundItem; // agarro el obj que esta atras de la fila seleccionada de la grilla 
                jugador1.Mazo.SacarCarta(pCarta);
                MessageBox.Show("se uso una carta " + pCarta.GetType());
                if (pCarta is Pato_Verde || pCarta is Pato_Azul)
                {
                    pCarta.Usar(jugador1);
                }
                else
                {
                    if(jugador1.Vida <= 5)
                    {
                        
                        pCarta.Usar(jugador1, jugador2);
                            
                    }
                    pCarta.Usar(jugador2);
                    
                }
                
                turnoJ1 = false;
                delegado = TurnoJ2;
                Actualizar();
            }

            else
            {
                delegado();
            }

        }

        private void UsarCartaJ2_Click(object sender, EventArgs e)
        {
            if(!turnoJ1)
            {
                // logica del uso de la carta ponerla aca adentro
                Carta pCarta = (Carta)gdrJugador2.SelectedRows[0].DataBoundItem; // agarro el obj que esta atras de la fila seleccionada de la grilla 
                jugador2.Mazo.SacarCarta(pCarta);
                MessageBox.Show("se uso una carta " + pCarta.GetType());
                if (pCarta is Pato_Verde || pCarta is Pato_Azul)
                {
                    pCarta.Usar(jugador2);
                }
                else
                {
                    if (jugador2.Vida <= 5)
                    {

                        pCarta.Usar(jugador2, jugador1);

                    }
                    pCarta.Usar(jugador1);
                }
                turnoJ1 = false;
                delegado = TurnoJ1;
                Actualizar();
                turnoJ1 = true;

                #region Control Rondas jugadas + CheckWin
                rondasJugadas++;
                MessageBox.Show(GameManager.CheckQuienTieneMasVida(jugador1, jugador2));
                MessageBox.Show(GameManager.CheckQuienTieneMasEscudo(jugador1, jugador2));
                if (rondasJugadas == 10)
                {
                    if (jugador1.Vida < jugador2.Vida || jugador1.Escudo < jugador2.Escudo)
                    {
                        MessageBox.Show("El Ganador es el Jugador 2");
                    }
                    if (jugador1.Vida > jugador2.Vida || jugador1.Escudo < jugador2.Escudo)
                    {
                        MessageBox.Show("El Ganador es el Jugador 1");
                    }
                    if (jugador1.Vida == jugador2.Vida || jugador1.Escudo < jugador2.Escudo)
                    {
                        MessageBox.Show("No hay ganador, tenemos un empate");
                    }
                }
                #endregion
                Actualizar();
            }

            else
            {
                delegado();
            }
        }

        private void AgarrarCarta1_Click(object sender, EventArgs e)
        {
            jugador1.Mazo.lCarta.Sort();
            jugador2.Mazo.lCarta.Sort();
            Actualizar();
        }
        private void gdrJugador2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }
        
        private void Reset_Click(object sender, EventArgs e)
        {
            Reiniciar();
        }

        void Reiniciar()
        {
            Application.Restart();
        }

        private void gdrJugador1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {   
            jugador1.Mazo.lCarta.Sort(new CompararValor());
            jugador2.Mazo.lCarta.Sort(new CompararValor());
            Actualizar();
        }

        void TurnoJ1()
        {
            MessageBox.Show("Es el turno del Jugador 1");
        }
        void TurnoJ2()
        {
            MessageBox.Show("Es el turno del Jugador 2");
        }

        private void progressBar2_Click(object sender, EventArgs e)
        {

        }
    }
}
