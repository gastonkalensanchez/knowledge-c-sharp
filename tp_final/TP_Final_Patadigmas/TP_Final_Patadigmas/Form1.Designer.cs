﻿
namespace TP_Final_Patadigmas
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.UsarCartaJ1 = new System.Windows.Forms.Button();
            this.UsarCartaJ2 = new System.Windows.Forms.Button();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.AgarrarCarta1 = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.progressBar3 = new System.Windows.Forms.ProgressBar();
            this.progressBar4 = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.gdrJugador1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.gdrJugador2 = new System.Windows.Forms.DataGridView();
            this.Reset = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gdrJugador1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gdrJugador2)).BeginInit();
            this.SuspendLayout();
            // 
            // UsarCartaJ1
            // 
            this.UsarCartaJ1.Location = new System.Drawing.Point(130, 268);
            this.UsarCartaJ1.Name = "UsarCartaJ1";
            this.UsarCartaJ1.Size = new System.Drawing.Size(113, 36);
            this.UsarCartaJ1.TabIndex = 7;
            this.UsarCartaJ1.Text = "Usar Carta";
            this.UsarCartaJ1.UseVisualStyleBackColor = true;
            this.UsarCartaJ1.Click += new System.EventHandler(this.SeleccionarCarta2J1_Click);
            // 
            // UsarCartaJ2
            // 
            this.UsarCartaJ2.Location = new System.Drawing.Point(868, 235);
            this.UsarCartaJ2.Name = "UsarCartaJ2";
            this.UsarCartaJ2.Size = new System.Drawing.Size(113, 36);
            this.UsarCartaJ2.TabIndex = 10;
            this.UsarCartaJ2.Text = "Usar Carta";
            this.UsarCartaJ2.UseVisualStyleBackColor = true;
            this.UsarCartaJ2.Click += new System.EventHandler(this.UsarCartaJ2_Click);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(483, 126);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(189, 256);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 12;
            this.pictureBox7.TabStop = false;
            // 
            // AgarrarCarta1
            // 
            this.AgarrarCarta1.Location = new System.Drawing.Point(484, 388);
            this.AgarrarCarta1.Name = "AgarrarCarta1";
            this.AgarrarCarta1.Size = new System.Drawing.Size(188, 46);
            this.AgarrarCarta1.TabIndex = 16;
            this.AgarrarCarta1.Text = "Ordenar Cartas por Color";
            this.AgarrarCarta1.UseVisualStyleBackColor = true;
            this.AgarrarCarta1.Click += new System.EventHandler(this.AgarrarCarta1_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 12);
            this.progressBar1.Maximum = 10;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(332, 67);
            this.progressBar1.TabIndex = 17;
            // 
            // progressBar2
            // 
            this.progressBar2.Location = new System.Drawing.Point(12, 85);
            this.progressBar2.Maximum = 10;
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(332, 67);
            this.progressBar2.TabIndex = 18;
            this.progressBar2.Click += new System.EventHandler(this.progressBar2_Click);
            // 
            // progressBar3
            // 
            this.progressBar3.Location = new System.Drawing.Point(796, 388);
            this.progressBar3.MarqueeAnimationSpeed = 10;
            this.progressBar3.Maximum = 10;
            this.progressBar3.Name = "progressBar3";
            this.progressBar3.Size = new System.Drawing.Size(332, 67);
            this.progressBar3.TabIndex = 19;
            // 
            // progressBar4
            // 
            this.progressBar4.Location = new System.Drawing.Point(796, 461);
            this.progressBar4.Maximum = 10;
            this.progressBar4.Name = "progressBar4";
            this.progressBar4.Size = new System.Drawing.Size(332, 67);
            this.progressBar4.TabIndex = 20;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "ESCUDO JUGADOR 1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(803, 400);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "VIDA JUGADOR 2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(803, 472);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "ESCUDO JUGADOR 2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(887, 303);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "JUGADOR 2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(175, 235);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "JUGADOR 1";
            // 
            // gdrJugador1
            // 
            this.gdrJugador1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdrJugador1.Location = new System.Drawing.Point(12, 340);
            this.gdrJugador1.Name = "gdrJugador1";
            this.gdrJugador1.Size = new System.Drawing.Size(355, 166);
            this.gdrJugador1.TabIndex = 29;
            this.gdrJugador1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gdrJugador1_CellContentClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 30;
            this.label1.Text = "VIDA JUGADOR 1";
            // 
            // gdrJugador2
            // 
            this.gdrJugador2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdrJugador2.Location = new System.Drawing.Point(773, 22);
            this.gdrJugador2.Name = "gdrJugador2";
            this.gdrJugador2.Size = new System.Drawing.Size(355, 166);
            this.gdrJugador2.TabIndex = 31;
            this.gdrJugador2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gdrJugador2_CellContentClick);
            // 
            // Reset
            // 
            this.Reset.Location = new System.Drawing.Point(501, 510);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(170, 32);
            this.Reset.TabIndex = 32;
            this.Reset.Text = "Reiniciar Juego";
            this.Reset.UseVisualStyleBackColor = true;
            this.Reset.Click += new System.EventHandler(this.Reset_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(483, 439);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(188, 46);
            this.button1.TabIndex = 33;
            this.button1.Text = "Ordenar Cartas por Valor";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(1152, 544);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Reset);
            this.Controls.Add(this.gdrJugador2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gdrJugador1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.progressBar4);
            this.Controls.Add(this.progressBar3);
            this.Controls.Add(this.progressBar2);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.AgarrarCarta1);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.UsarCartaJ2);
            this.Controls.Add(this.UsarCartaJ1);
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gdrJugador1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gdrJugador2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button UsarCartaJ1;
        private System.Windows.Forms.Button UsarCartaJ2;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Button AgarrarCarta1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.ProgressBar progressBar3;
        private System.Windows.Forms.ProgressBar progressBar4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView gdrJugador1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView gdrJugador2;
        private System.Windows.Forms.Button Reset;
        private System.Windows.Forms.Button button1;
    }
}

