﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class Pato_Rojo : Carta, IPato
    {
        public Pato_Rojo()
        {
            Color = color.Rojo;
            Valor = rand.Next(1, 6);
            TipoDeCarta = Constante.tipoDeCarta.Daño;
        }

        public Constante.tipoDeCarta TipoDeCarta { get; set; }

        public override void Usar(Jugador jugador)
        {
            //Ataca
            if (jugador.Escudo > 0)
            {
               int DañoEscudo = jugador.Escudo -= Valor; // me fijo cuanto le saco al escudo y lo guardo en una variable
                jugador.Escudo = DañoEscudo;                                      
                // si tengo 10 de escudo y valor es 15 va a dar -5
                if (DañoEscudo < 0)
                {
                    jugador.Escudo = 0;
                    jugador.Vida += DañoEscudo;
                }
            }
            else
            {
                jugador.Vida -= Valor;               
            }
        }
        public override void Usar(Jugador JugadorAtaca,Jugador jugadorDefiende)
        {
            JugadorAtaca.Vida += Valor;
            jugadorDefiende.Vida -= Valor;
        }
    }
}
