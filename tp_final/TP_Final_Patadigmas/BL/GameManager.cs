﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class GameManager
    {
        public static string CheckQuienTieneMasVida(Jugador jugador1, Jugador jugador2) //Metodo static donde se ve si tienen vida o no,
        {
            
            if (jugador1.Vida == jugador2.Vida)
            {
                return "Tienen La misma vida";
            }
            if( jugador1.Vida > jugador2.Vida)
            {
                return "El jugador 1 tiene mas vida";
            }
           
            if (jugador1.Vida < jugador2.Vida)
            {
                return "El jugador 2 tiene mas vida";
            }

            return "";
        }
        public static string CheckQuienTieneMasEscudo(Jugador jugador1, Jugador jugador2) //Metodo static donde se ve si tienen vida o no,
        {
            if (jugador1.Escudo == jugador2.Escudo)
            {
                return "Tienen el mismo escudo";
            }
                       
            if (jugador1.Escudo > jugador2.Escudo)
            {
                return "El jugador 1 tiene mas escudo";
            }
            if (jugador1.Escudo < jugador2.Escudo)
            {
                return "El jugador 2 tiene mas escudo";
            }

            return "";
        }
    }
}
