﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class Mazo : IEnumerable<Carta> 
    {
        private List<Carta> mCartas = new List<Carta>();

        public delegate void Action(Jugador jugador);
        Action a;

        public void Agregar(Carta pCarta)
        {
            mCartas.Add(pCarta);
            a += pCarta.Usar;
        }
        public void SacarCarta(Carta pCarta)
        {
            mCartas.Remove(pCarta);
        }

        public IEnumerator<Carta> GetEnumerator()
        {
            return mCartas.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return mCartas.GetEnumerator();
        }
        public List<Carta> lCarta { get { return mCartas; } }

        //public IEnumerator<Carta> GetEnumerator()
        //{
        //    return mCartas.GetEnumerator();
        //}

        //IEnumerator IEnumerable.GetEnumerator()
        //{
        //    return mCartas.GetEnumerator();
        //}

        //    public class EnumeradorPersonalizado : IEnumerator<Carta>
        //    {

        //        int indice = -1;

        //        Mazo mMazo = null;

        //        public EnumeradorPersonalizado(Mazo pMazo)
        //        {
        //            mMazo = pMazo;
        //        }

        //        public Carta Current 
        //        {  
        //            get 
        //            {

        //                Pato_Amarillo cartaCopiaAmarillo = new Pato_Amarillo();
        //                //Pato_Azul cartaCopiaAzul = new Pato_Azul();
        //                //Pato_Rojo cartaCopiaRoja = new Pato_Rojo();

        //                if(mMazo.mCartas[indice].color == 0)
        //                {
        //                    Carta cartaCopiaVerde = new Pato_Verde();
        //                    cartaCopiaVerde.color = mMazo.mCartas[indice].color;
        //                    cartaCopiaVerde.Valor = mMazo.mCartas[indice].Valor;
        //                    return cartaCopiaVerde;
        //                }
        //                return mMazo.mCartas[indice];
        //            } 

        //        }


        //        object IEnumerator.Current  
        //        {
        //            get
        //            {
        //                if (mMazo.mCartas[indice].color == 0)
        //                {
        //                    Pato_Verde cartaCopiaVerde = new Pato_Verde();
        //                    cartaCopiaVerde.color = mMazo.mCartas[indice].color;
        //                    cartaCopiaVerde.Valor = mMazo.mCartas[indice].Valor;
        //                    return cartaCopiaVerde;
        //                }
        //                return mMazo.mCartas[indice];
        //            }
        //        }

        //        public void Dispose()
        //        {
        //        }

        //        public bool MoveNext()
        //        {
        //            indice += 1;
        //            if (indice >= mMazo.mCartas.Count)
        //                return false;
        //            return true;
        //        }

        //        public void Reset()
        //        {
        //           indice = -1;
        //        }
        //    }
        //}
    }
}
