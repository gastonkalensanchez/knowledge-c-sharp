﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class Pato_Verde : Carta, IPato
    {
        public Pato_Verde()
        {
            Color = color.Verde;
            Valor = rand.Next(1, 6);
            TipoDeCarta = Constante.tipoDeCarta.Curacion;
        }

        public Constante.tipoDeCarta TipoDeCarta { get; set; }

        public override void Usar(Jugador jugador)
        { 
            //Suma Vida (Curacion)
            int nuevaVida = jugador.Vida += Valor;

            if(nuevaVida >= 10)
            {
                jugador.Vida = 10;
            }
            
        }
    }
}
