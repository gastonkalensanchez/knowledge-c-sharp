﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    
    public abstract class Carta : IComparable<Carta>
    {
        public int Valor { get; set; }
        public enum color { Verde, Amarillo, Azul , Rojo }
        public color Color { get; set; }             

        public static Random rand = new Random();

        public abstract void Usar(Jugador jugador);
        public virtual void Usar(Jugador jugador, Jugador jugador1)
        {

        }
        public int CompareTo(Carta other) // que compare las cartas por valor 
        {
            return Color.CompareTo(other.Color);
        }
    }
    public class CompararValor : IComparer<Carta>
    {
        public int Compare(Carta x, Carta y)
        {
            return x.Valor.CompareTo(y.Valor);
        }
    }
}
