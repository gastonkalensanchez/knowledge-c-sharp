﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class Pato_Amarillo : Carta, IPato
    {
        public Pato_Amarillo()
        {
            Color = color.Amarillo;
            Valor = rand.Next(1, 6);
            TipoDeCarta = Constante.tipoDeCarta.Especial;
        }

        public Constante.tipoDeCarta TipoDeCarta { get; set; }

        public void Potenciar()
        {
            Valor = Valor * 2;
        }

        public override void Usar(Jugador jugador)
        {
            Potenciar();      
        }
    }
}
