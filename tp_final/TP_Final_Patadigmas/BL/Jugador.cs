﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class Jugador
    {
        public int Escudo { get; set; }
        public int Vida { get; set; }
        public int CantCartas { get; set; }

        public delegate void UsarCarta();

        UsarCarta c;

        public Mazo Mazo { get; set; }
    }
}
