﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class Pato_Azul : Carta, IPato
    {
       
        public Pato_Azul()
        {    
            Color = color.Azul;
            Valor = rand.Next(1, 6);
            TipoDeCarta = Constante.tipoDeCarta.Escudo;
        }

        public Constante.tipoDeCarta TipoDeCarta { get; set; }

        public override void Usar(Jugador jugador)
        {
            int nuevoEscudo = jugador.Escudo += Valor;

            if (nuevoEscudo >= 10)
            {
                jugador.Escudo = 10;
            }

        }
    }
}
